import { createApp } from "vue";
import App from "./App.vue";
import router from "./router";
import { Menu, Input, Layout, Button, Card, Col, Row, Form } from "ant-design-vue";
import './assets/css/index.css';

const app = createApp(App);

app.config.productionTip = false;
app.use(router)
app.use(Input);
app.use(Menu);
app.use(Layout);
app.use(Button);
app.use(Card);
app.use(Col);
app.use(Row);
app.use(Form);

app.mount("#app");
