import axios from "axios";
const emailRegex = /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
export default {
  data: () => {
    return {
      loading: false,
      form: {
        email: "",
        password: "",
        confirm: "",
      },
      emailError: false,
      passError: false,
    };
  },
  inject: ["loggedIn"],
  methods: {
    invalidEmail() {
      this.emailError = false;
      if (!emailRegex.test(this.form.email)) {
        this.emailError = true;
        return true;
      }
      return false;
    },
    invalidPass() {
      this.passError = false;
      if (this.form.password.length < 1) {
        this.passError = true;
        return true;
      }
      return false;
    },
    invalidConfirmPass() {
      this.passError = false;
      if (this.form.password !== this.form.confirm) {
        this.passError = true;
        return true;
      }
      return false;
    },
    async sendRequest(url, isRegister = false) {
      this.loading = true;
      this.invalidEmail();
      this.invalidPass();
      isRegister && this.invalidConfirmPass();
      if (this.emailError || this.passError) {
        this.loading = false;
        return;
      }

      try {
        const response = await axios.post(url, this.form);
        this.form = { email: "", password: "" };
        this.loggedIn(response.data);
        this.loading = false;
      } catch (e) {
        console.log("ERROR >>>", e);
        this.loading = false;
      }
    },
  },
};
