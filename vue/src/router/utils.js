const TOKEN = "token";
const EXPIRES = "token_expires";
const USER = "user";

export const redirectLogin = (to) => ({
  name: "Login",
  params: { nextUrl: to.fullPath },
});

export const validAuth = () => {
  const user = JSON.parse(localStorage.getItem(USER));
  const token = localStorage.getItem(TOKEN);
  const token_expires = localStorage.getItem(EXPIRES);
  if (!user || !token || !token_expires) return false;
  if (token_expires <= Date.now()) {
    removeSession();
    return false;
  }
  return true;
};

export const createSession = ({ user, token, token_expires }) => {
  if (!user || !token || !token_expires) return false;
  localStorage.setItem(USER, JSON.stringify(user));
  localStorage.setItem(TOKEN, token);
  //make it in milliseconds (check validAuth)
  localStorage.setItem(EXPIRES, token_expires * 1000);
  return true;
};

export const removeSession = () => {
  localStorage.removeItem(USER);
  localStorage.removeItem(TOKEN);
  localStorage.removeItem(EXPIRES);
};

export const leftToExpire = () => {
  const expiresAt = localStorage.getItem(EXPIRES);
  if (!expiresAt) return 0;
  const diff = expiresAt - Date.now();
  if (diff <= 0) return 0;
  return diff;
};
