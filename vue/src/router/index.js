import { createRouter, createWebHistory } from "vue-router";
import Home from "../views/Home.vue";
import { validAuth, redirectLogin } from "./utils";

const routes = [
  {
    path: "/",
    name: "Home",
    component: Home,
    meta: { requiresAuth: true },
  },
  {
    path: "/login",
    name: "Login",
    meta: { guest: true },
    component: () =>
      import(/* webpackChunkName: "login" */ "../views/Login.vue"),
  },
  {
    path: "/register",
    name: "Register",
    meta: { guest: true },
    component: () =>
      import(/* webpackChunkName: "register" */ "../views/Register.vue"),
  },
];

const router = createRouter({
  history: createWebHistory(process.env.BASE_URL),
  routes,
});

router.beforeEach((to, _, next) => {
  if (to.matched.some((record) => record.meta.requiresAuth)) {
    if (!validAuth()) next(redirectLogin(to));
    else next();
  } else if (to.matched.some((record) => record.meta.guest)) {
    if (validAuth()) next({ name: "Home" });
    else next();
  } else next(); //not protected or guest pages
});

export default router;
