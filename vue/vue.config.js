module.exports = {
  css: {
    loaderOptions: {
      less: {
        lessOptions: {
          javascriptEnabled: true,
          modifyVars: {
            "primary-color": "#faa41f",
            "text-color-secondary":"#f5970a",
            "heading-color":"burlywood",
            "link-color": "white",
            "font-size-base":"18px",
            "border-radius-base": "30px",
          },
        },
      },
    },
  },
};
