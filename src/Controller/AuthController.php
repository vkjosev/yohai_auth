<?php

namespace App\Controller;

use App\Entity\User;
use App\Repository\UserRepository;
use Exception;
use Firebase\JWT\JWT;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Validator\Validator\ValidatorInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

// ** Tutorial used
// ** https://smoqadam.me/posts/how-to-authenticate-user-in-symfony-5-by-jwt/ 

class AuthController extends AbstractController
{
    private $email;
    private  $pass;

    private function invalid($request)
    {
        $this->email = $request->get('email');
        $this->pass = $request->get('password');
        return empty($this->email) || empty($this->pass);
    }

    private function createJwtResponse(User $user)
    {
        $expires = strtotime("+60 minutes");
        $id = $user->getId();
        $email = $user->getEmail();
        $jwt = JWT::encode(
            [
                "user" => $email,
                "exp"  => $expires
            ],
            $this->getParameter('jwt_secret'),
            'HS256'
        );

        return $this->json(['success' => true, 'token' => $jwt, 'token_expires' => $expires, "user" => ["id" => $id, "email" => $email]]);
    }

    #[Route('/auth/login', name: 'login', methods: ['POST'])]
    public function login(Request $request, UserRepository $userRepository, UserPasswordEncoderInterface $encoder): Response
    {
        try {
            if ($this->invalid($request))
                return $this->json(['success' => false, 'message' => 'Data is missing'], Response::HTTP_BAD_REQUEST);
            $user = $userRepository->findOneBy(['email' => $this->email]);
            //check user exists and password correct
            if (!$user || !$encoder->isPasswordValid($user, $this->pass)) {
                return $this->json([
                    'success' => false,  'message' => 'email or password is wrong.',
                ], Response::HTTP_UNAUTHORIZED);
            }

            return $this->createJwtResponse($user);
        } catch (Exception $e) {
            //you can use logger for $e
            return $this->json(['success' => false, 'message' => 'Failed to login'], Response::HTTP_INTERNAL_SERVER_ERROR);
        }
    }

    #[Route('/auth/register', name: 'register', methods: ['POST'])]
    public function register(Request $request, UserPasswordEncoderInterface $encoder, ValidatorInterface $validator): Response
    {
        try {
            if ($this->invalid($request))
                return $this->json(['success' => false, 'message' => 'Data is empty'], Response::HTTP_BAD_REQUEST);
            $user = new User();
            $user->setPassword($encoder->encodePassword($user, $this->pass));
            $user->setEmail($this->email);
            //validate against the entity (unique email, notBlank password)
            $errors = $validator->validate($user);
            if (count($errors) > 0)
                return $this->json(['success' => false, 'message' => $errors->get(0)->getMessage()], 400);

            $em = $this->getDoctrine()->getManager();
            $em->persist($user);
            $em->flush();
            return $this->createJwtResponse($user);
        } catch (Exception $e) {
            //you can use logger for $e
            return $this->json(['success' => false, 'message' => 'Something went wrong'], Response::HTTP_INTERNAL_SERVER_ERROR);
        }
    }
}
