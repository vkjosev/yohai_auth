<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\User\UserInterface;

class ApiController extends AbstractController
{
    #[Route('/api/me', name: 'getUser')]
    public function test(UserInterface $user)
    {
        return $this->json([
            'success' => true,
            'email' => $user->getUsername(),
        ]);
    }
}
