# Symfony (backend)

## Project setup
Install the PHP dependencies. Check *CORS* allowed origin in `.env` file to contain the right string/regex
```
composer install
```

## Database setup
Change the Database string in `.env` file to the desired one
```
symfony console doctrine:migration:migrate
```

### Compiles and hot-reloads the backend for development
```
symfony serve --no-tls
```

# Vue (frontend)

Change first the working directory by typing in the terminal `cd vue`, because Vue frontend project is in that subfolder
## Project setup
```
yarn install
```

### Compiles and hot-reloads the backend for development
```
yarn serve
```